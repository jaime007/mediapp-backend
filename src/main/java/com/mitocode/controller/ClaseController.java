package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Clase;
import com.mitocode.model.Signo;
import com.mitocode.service.IClaseService;

@RestController
@RequestMapping("/clases")
public class ClaseController {

	@Autowired
	private IClaseService service;

	@GetMapping
	public ResponseEntity<List<Clase>> listar() throws Exception {
		List<Clase> lista = service.listar();
		return new ResponseEntity<List<Clase>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Clase> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Clase obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Clase>(obj, HttpStatus.OK);
	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Clase> listarPorIdHateoas(@PathVariable("id") Integer id) throws Exception {
		Clase obj = service.listarPorId(id);

		if (obj.getIdClase() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		// localhost:8080/Clases/{id}
		EntityModel<Clase> recurso = EntityModel.of(obj);

		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));

		recurso.add(linkTo.withRel("Clase-recurso"));

		return recurso;
	}

	/*
	 * @PostMapping public ResponseEntity<Clase> registrar(@Valid @RequestBody
	 * Clase p) { Clase obj = service.registrar(p); return new
	 * ResponseEntity<Clase>(obj, HttpStatus.CREATED); }
	 */

	@PostMapping
	public ResponseEntity<Clase> registrar(@Valid @RequestBody Clase p) throws Exception {
		Clase obj = service.registrar(p);

		// localhost:8080/Clases/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdClase()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Clase> modificar(@Valid @RequestBody Clase p) throws Exception {
		Clase obj = service.modificar(p);
		return new ResponseEntity<Clase>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Clase obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<Clase>> listarPageable(Pageable pageable) throws Exception{
		Page<Clase> clases = service.listarPageable(pageable);
		return new ResponseEntity<Page<Clase>>(clases, HttpStatus.OK);
	}
}
