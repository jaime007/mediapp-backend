package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Color;
import com.mitocode.service.IColorService;

@RestController
@RequestMapping("/colores")
public class ColorController {

	@Autowired
	private IColorService service;

	@GetMapping
	public ResponseEntity<List<Color>> listar() throws Exception {
		List<Color> lista = service.listar();
		return new ResponseEntity<List<Color>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Color> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Color obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Color>(obj, HttpStatus.OK);
	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Color> listarPorIdHateoas(@PathVariable("id") Integer id) throws Exception {
		Color obj = service.listarPorId(id);

		if (obj.getIdColor() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		// localhost:8080/Colors/{id}
		EntityModel<Color> recurso = EntityModel.of(obj);

		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));

		recurso.add(linkTo.withRel("Color-recurso"));

		return recurso;
	}

	/*
	 * @PostMapping public ResponseEntity<Color> registrar(@Valid @RequestBody
	 * Color p) { Color obj = service.registrar(p); return new
	 * ResponseEntity<Color>(obj, HttpStatus.CREATED); }
	 */

	@PostMapping
	public ResponseEntity<Color> registrar(@Valid @RequestBody Color p) throws Exception {
		Color obj = service.registrar(p);

		// localhost:8080/Colors/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdColor()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Color> modificar(@Valid @RequestBody Color p) throws Exception {
		Color obj = service.modificar(p);
		return new ResponseEntity<Color>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Color obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
