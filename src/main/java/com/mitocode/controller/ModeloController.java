package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Modelo;
import com.mitocode.service.IModeloService;

@RestController
@RequestMapping("/modelos")
public class ModeloController {

	@Autowired
	private IModeloService service;

	@GetMapping
	public ResponseEntity<List<Modelo>> listar() throws Exception {
		List<Modelo> lista = service.listar();
		return new ResponseEntity<List<Modelo>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Modelo> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Modelo obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Modelo>(obj, HttpStatus.OK);
	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Modelo> listarPorIdHateoas(@PathVariable("id") Integer id) throws Exception {
		Modelo obj = service.listarPorId(id);

		if (obj.getIdModelo() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		// localhost:8080/Modelos/{id}
		EntityModel<Modelo> recurso = EntityModel.of(obj);

		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));

		recurso.add(linkTo.withRel("Modelo-recurso"));

		return recurso;
	}

	/*
	 * @PostMapping public ResponseEntity<Modelo> registrar(@Valid @RequestBody
	 * Modelo p) { Modelo obj = service.registrar(p); return new
	 * ResponseEntity<Modelo>(obj, HttpStatus.CREATED); }
	 */

	@PostMapping
	public ResponseEntity<Modelo> registrar(@Valid @RequestBody Modelo p) throws Exception {
		Modelo obj = service.registrar(p);

		// localhost:8080/Modelos/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdModelo()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Modelo> modificar(@Valid @RequestBody Modelo p) throws Exception {
		Modelo obj = service.modificar(p);
		return new ResponseEntity<Modelo>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Modelo obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
