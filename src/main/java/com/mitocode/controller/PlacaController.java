package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Placa;
import com.mitocode.service.IPlacaService;

@RestController
@RequestMapping("/placas")
public class PlacaController {

	@Autowired
	private IPlacaService service;

	@GetMapping
	public ResponseEntity<List<Placa>> listar() throws Exception {
		List<Placa> lista = service.listar();
		return new ResponseEntity<List<Placa>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Placa> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Placa obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Placa>(obj, HttpStatus.OK);
	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Placa> listarPorIdHateoas(@PathVariable("id") Integer id) throws Exception {
		Placa obj = service.listarPorId(id);

		if (obj.getIdPlaca() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		// localhost:8080/Placas/{id}
		EntityModel<Placa> recurso = EntityModel.of(obj);

		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));

		recurso.add(linkTo.withRel("Placa-recurso"));

		return recurso;
	}

	/*
	 * @PostMapping public ResponseEntity<Placa> registrar(@Valid @RequestBody
	 * Placa p) { Placa obj = service.registrar(p); return new
	 * ResponseEntity<Placa>(obj, HttpStatus.CREATED); }
	 */

	@PostMapping
	public ResponseEntity<Placa> registrar(@Valid @RequestBody Placa p) throws Exception {
		Placa obj = service.registrar(p);

		// localhost:8080/Placas/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdPlaca()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Placa> modificar(@Valid @RequestBody Placa p) throws Exception {
		Placa obj = service.modificar(p);
		return new ResponseEntity<Placa>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Placa obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
