package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "placa")
public class Placa {

	@Id
	private Integer idPlaca;

	@Column(name = "num_placa")
	private String numPlaca;

	@ManyToOne
	@JoinColumn(name = "id_clase", nullable = false, foreignKey = @ForeignKey(name = "FK_placa_clase"))
	private Clase clase;
	
	@ManyToOne
	@JoinColumn(name = "id_marca", nullable = false, foreignKey = @ForeignKey(name = "FK_placa_marca"))
	private Marca marca;
	
	@ManyToOne
	@JoinColumn(name = "id_modelo", nullable = false, foreignKey = @ForeignKey(name = "FK_placa_modelo"))
	private Modelo modelo;
	
	@ManyToOne
	@JoinColumn(name = "id_color", nullable = false, foreignKey = @ForeignKey(name = "FK_placa_color"))
	private Color color;
	
	
	@ManyToOne
	@JoinColumn(name = "id_propietario", nullable = false, foreignKey = @ForeignKey(name = "FK_placa_propietario"))
	private Propietario propietario;	
	
	@Column(name = "anio")
	private String anio;
	
	@Column(name = "fecha_emision", nullable = false)
	private LocalDateTime fechaEmision;
	
	@Column(name = "fecha_vencimiento", nullable = false)
	private LocalDateTime fechaVencimiento;
	
	
	public Integer getIdPlaca() {
		return idPlaca;
	}

	public void setIdPlaca(Integer idPlaca) {
		this.idPlaca = idPlaca;
	}
	
	public String getNumPlaca() {
		return numPlaca;
	}

	public void setNumPlaca(String numPlaca) {
		this.numPlaca = numPlaca;
	}
		
	public Clase getClase() {
		return clase;
	}

	public void setClase(Clase clase) {
		this.clase = clase;
	}
	
	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}	
	
	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}	
	
	public Propietario getPropietario() {
		return propietario;
	}

	public void setPropietario(Propietario propietario) {
		this.propietario = propietario;
	}	
	
	public LocalDateTime getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(LocalDateTime fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	
	public LocalDateTime getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDateTime fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}	
	
	
	
}