package com.mitocode.repo;

import com.mitocode.model.Clase;

public interface IClaseRepo extends IGenericRepo<Clase, Integer> {

}
