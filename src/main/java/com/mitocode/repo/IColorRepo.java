package com.mitocode.repo;

import com.mitocode.model.Color;

public interface IColorRepo extends IGenericRepo<Color, Integer> {

}
