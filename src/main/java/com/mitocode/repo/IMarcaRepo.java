package com.mitocode.repo;

import com.mitocode.model.Marca;

public interface IMarcaRepo extends IGenericRepo<Marca, Integer> {

}
