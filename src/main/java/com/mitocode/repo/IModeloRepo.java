package com.mitocode.repo;

import com.mitocode.model.Modelo;

public interface IModeloRepo extends IGenericRepo<Modelo, Integer> {

}
