package com.mitocode.repo;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Paciente;

//@Repository
public interface IPacienteRepo extends IGenericRepo<Paciente, Integer>{
	
	@Query("FROM Paciente p WHERE p.dni = :dni")
	Paciente listarPorDni(@Param("dni") String dni);

}