package com.mitocode.repo;

import com.mitocode.model.Placa;

public interface IPlacaRepo extends IGenericRepo<Placa, Integer> {

}
