package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Clase;

public interface IClaseService extends ICRUD<Clase, Integer> {
	Page<Clase> listarPageable(Pageable pageable);

}
