package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Color;

public interface IColorService extends ICRUD<Color, Integer> {
	
	Page<Color> listarPageable(Pageable pageable);

}
