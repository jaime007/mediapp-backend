package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Marca;

public interface IMarcaService extends ICRUD<Marca, Integer> {
	
	Page<Marca> listarPageable(Pageable pageable);

}
