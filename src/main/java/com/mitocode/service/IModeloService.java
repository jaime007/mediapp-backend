package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Modelo;

public interface IModeloService extends ICRUD<Modelo, Integer> {

	Page<Modelo> listarPageable(Pageable pageable);
	
}
