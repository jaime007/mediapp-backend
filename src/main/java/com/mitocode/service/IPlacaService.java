package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Placa;

public interface IPlacaService extends ICRUD<Placa, Integer> {
	
	Page<Placa> listarPageable(Pageable pageable);

}
