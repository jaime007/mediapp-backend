package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Clase;
import com.mitocode.repo.IClaseRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IClaseService;

@Service
public class ClaseServiceImpl extends CRUDImpl<Clase, Integer> implements IClaseService {
	
	@Autowired
	private IClaseRepo repo;

	@Override
	public Page<Clase> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	protected IGenericRepo<Clase, Integer> getRepo() {
		return repo;
	}

}
