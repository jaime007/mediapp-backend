package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Color;
import com.mitocode.repo.IColorRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IColorService;

@Service
public class ColorServiceImpl extends CRUDImpl<Color, Integer> implements IColorService {

	
	@Autowired
	private IColorRepo repo;

	@Override
	public Page<Color> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	protected IGenericRepo<Color, Integer> getRepo() {
		return repo;
	}

}