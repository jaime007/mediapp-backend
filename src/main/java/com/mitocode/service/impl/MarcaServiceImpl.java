package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Marca;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IMarcaRepo;
import com.mitocode.service.IMarcaService;

@Service
public class MarcaServiceImpl extends CRUDImpl<Marca, Integer> implements IMarcaService {

	
	@Autowired
	private IMarcaRepo repo;

	@Override
	public Page<Marca> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	protected IGenericRepo<Marca, Integer> getRepo() {
		return repo;
	}

}
