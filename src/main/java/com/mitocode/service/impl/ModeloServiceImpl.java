package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Modelo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IModeloRepo;
import com.mitocode.service.IModeloService;

@Service
public class ModeloServiceImpl extends CRUDImpl<Modelo, Integer> implements IModeloService {

	
	@Autowired
	private IModeloRepo repo;

	@Override
	public Page<Modelo> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	protected IGenericRepo<Modelo, Integer> getRepo() {
		return repo;
	}

}
