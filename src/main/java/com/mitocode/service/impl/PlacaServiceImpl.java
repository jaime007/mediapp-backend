package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Placa;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IPlacaRepo;
import com.mitocode.service.IPlacaService;

@Service
public class PlacaServiceImpl extends CRUDImpl<Placa, Integer> implements IPlacaService {

	
	@Autowired
	private IPlacaRepo repo;

	@Override
	public Page<Placa> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	protected IGenericRepo<Placa, Integer> getRepo() {
		return repo;
	}

}